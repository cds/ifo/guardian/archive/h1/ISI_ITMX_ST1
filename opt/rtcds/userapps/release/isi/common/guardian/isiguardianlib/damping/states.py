# The defaultdict class is used in the ENGAGE_DAMPING_LOOPS state to track
# which of the filters we want to engage are actually loaded. The default value
# used for the dictionary that tracks this is of course the empty list.
#
# For more details on the defaultdict class, refer to
# http://docs.python.org/2/library/collections.html#collections.defaultdict
from collections import defaultdict

from guardian import GuardState
from ezca.ligofilter import LIGOFilterManager
from ezca.ligofilter import LIGOFilterError
from ezca.ezca import EzcaError

from . import const as damp_const
from .. import util as top_util
from .. import const as top_const
from .. import decorators as dec
from ..isolation import util as iso_util
from ..isolation import const as iso_const


class ENGAGE_DAMPING_LOOPS(GuardState):
    request = False
    index = 41

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_off
    @dec.isolation_loops_are_off
    def main(self):
        self.filter_manager = LIGOFilterManager(['DAMP_'+dof for dof in damp_const.DAMPING_CONSTANTS['ALL_DOF']], ezca)
        self.has_pressed_buttons = False
        self.loaded_filter_modules = defaultdict(list)

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.isolation_loops_are_off
    def run(self):
        try:
            if not self.has_pressed_buttons:
                # For each LIGOFilter instance associated with the
                # LIGOFilterManager instance constructed in self.main(), find
                # out which of the damping filters are loaded. This state will
                # engage all of the filters that are loaded in the modules
                # defined in damp_const.DAMPING_CONSTANTS['FILTER_NAMES'].
                # This check is important because it prevents the guardian from
                # trying to engage filter modules that do not have filters
                # loaded (e.g. an empty FM1 module).
                for ligo_filter in self.filter_manager.filter_dict.values():
                    self.loaded_filter_modules[ligo_filter.filter_name] =\
                            top_util.get_loaded_filter_modules(ligo_filter, damp_const.DAMPING_CONSTANTS['FILTER_NAMES'])
                    if self.loaded_filter_modules[ligo_filter.filter_name]:
                        ligo_filter.only_on(*tuple(list(damp_const.DAMPING_CONSTANTS['ONLY_ON_BUTTONS'])\
                                + self.loaded_filter_modules[ligo_filter.filter_name]))
                self.has_pressed_buttons = True

            for ligo_filter in self.filter_manager.filter_dict.values():
                if self.loaded_filter_modules[ligo_filter.filter_name]:
                    for button in damp_const.DAMPING_CONSTANTS['ONLY_ON_BUTTONS']\
                            + self.loaded_filter_modules[ligo_filter.filter_name]:
                        if not ligo_filter.is_engaged(button):
                            return False
            return True

        except (LIGOFilterError, EzcaError) as e:
            top_util.report_exception(e)
            return False


class RAMP_DAMPING_FILTERS_UP(GuardState):
    request = False
    index = 42

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_in_preferred_state
    @dec.isolation_loops_are_off
    def main(self):
        self.filter_manager = LIGOFilterManager(['DAMP_'+dof for dof in damp_const.DAMPING_CONSTANTS['ALL_DOF']], ezca)
        self.has_started_ramping_gains = False

    # Note that we exclude @dec.damping_loops_are_in_preferred_state
    # here because the gain ramp could (will) be ramping here
    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.isolation_loops_are_off
    def run(self):
        try:
            if not self.has_started_ramping_gains:
                self.filter_manager.ramp_gains(gain_value=damp_const.DAMPING_CONSTANTS['GAIN'],
                                               ramp_time=damp_const.DAMPING_CONSTANTS['RAMP_UP_TIME'],
                                               wait=False)
                self.has_started_ramping_gains = True

            if self.filter_manager.any_gain_ramping():
                return False

            return True

        except (LIGOFilterError, EzcaError) as e:
            top_util.report_exception(e)
            return False


class RAMP_DAMPING_FILTERS_DOWN(GuardState):
    request = False
    index = 46

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_in_preferred_state
    @dec.isolation_loops_are_off
    def main(self):
        self.filter_manager = LIGOFilterManager(['DAMP_'+dof for dof in damp_const.DAMPING_CONSTANTS['ALL_DOF']], ezca)
        self.has_started_ramping_gains = False

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.isolation_loops_are_off
    def run(self):
        try:
            if not self.has_started_ramping_gains:
                self.filter_manager.ramp_gains(gain_value=0, ramp_time=damp_const.DAMPING_CONSTANTS['RAMP_DOWN_TIME'], wait=False)
                self.has_started_ramping_gains = True

            if not self.filter_manager.any_gain_ramping():
                return True
            return False

        except (LIGOFilterError, EzcaError) as e:
            top_util.report_exception(e)
            return False


class DISENGAGE_DAMPING_LOOPS(GuardState):
    request = False
    index = 47

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.isolation_loops_are_off
    def main(self):
        self.filter_manager = LIGOFilterManager(['DAMP_'+dof for dof in damp_const.DAMPING_CONSTANTS['ALL_DOF']], ezca)
        self.has_pressed_buttons = False

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.isolation_loops_are_off
    def run(self):
        try:
            if not self.has_pressed_buttons:
                self.filter_manager.all_do('all_off', wait=False)
                self.has_pressed_buttons = True

            # For each LIGOFilter instance associated with the
            # LIGOFilterManager instance constructed in self.main(), check
            # that all of the buttons we may have engaged are now disengaged.
            #
            # FIXME Should we check that the entire filter is disengaged?
            # Should that type of check be a method in the LIGOFilter class?
            for ligo_filter in self.filter_manager.filter_dict.values():
                for button_name in damp_const.DAMPING_CONSTANTS['FILTER_NAMES']\
                        + damp_const.DAMPING_CONSTANTS['ONLY_ON_BUTTONS']:
                    if ligo_filter.is_engaged(button_name):
                        return False

            return True

        except (LIGOFilterError, EzcaError) as e:
            top_util.report_exception(e)
            return False


class DAMPED(GuardState):
    request = True
    index = 50

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_in_preferred_state
    @dec.damping_loops_have_correct_gain
    @dec.isolation_loops_are_off
    def main(self):
        if (iso_const.ISOLATION_CONSTANTS['SWITCH_GS13_GAIN']) & (top_const.CHAMBER_TYPE != 'BSC_ST1') & (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.switch_gs13_gain('LOW', iso_const.ISOLATION_CONSTANTS['GS13_GAIN_DOF']) 

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_in_preferred_state
    @dec.damping_loops_have_correct_gain
    @dec.isolation_loops_are_off
    def run(self):
        return True
